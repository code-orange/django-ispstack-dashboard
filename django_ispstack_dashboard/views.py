from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from django_ispstack_main.django_ispstack_main.views import get_nav


def dashboard(request):
    if not request.user.is_authenticated:
        print("User ist nicht angemeldet")
        return HttpResponseRedirect("/account/login/")

    template = loader.get_template("django_ispstack_dashboard/dashboard.html")

    template_opts = dict()

    template_opts["content_title_main"] = "Dashboard"
    template_opts["content_title_sub"] = "Welcome"

    template_opts["menu_groups"] = get_nav()

    return HttpResponse(template.render(template_opts, request))
